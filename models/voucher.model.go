package models

import (
	"echo-rest/db"
	"net/http"
)

type Voucher struct {
	Id         int    `json:"id"`
	Nama       string `json:"nama"`
	Keterangan string `json:"keterangan"`
	Tanggal    string `json:"tanggal"`
}

func FetchAllVoucher() (Response, error) {
	var obj Voucher
	var arrobj []Voucher
	var res Response

	con := db.CreateCon()

	sqlStatement := "SELECT * FROM voucher"

	rows, err := con.Query(sqlStatement)
	defer rows.Close()

	if err != nil {
		return res, err
	}

	for rows.Next() {
		err = rows.Scan(&obj.Id, &obj.Nama, &obj.Keterangan, &obj.Tanggal)
		if err != nil {
			return res, err
		}

		arrobj = append(arrobj, obj)
	}

	res.Status = http.StatusOK
	res.Message = "Sukses"
	res.Data = arrobj

	return res, nil
}
