package controllers

import (
	"echo-rest/models"
	"net/http"

	"github.com/labstack/echo/v4"
)

func FetchAllVoucher(c echo.Context) error {
	result, err := models.FetchAllVoucher()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, result)
}
